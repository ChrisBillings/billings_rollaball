﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
    public Text countText;
    public Text winText;
    private int count;
    public AudioClip pickUpSound;
    public bool isGrounded;
    public float jumpPower = 0.3f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();

            transform.localScale += new Vector3(0.2F, 0.2F, 0.2F);
            //makes sphere bigger
            speed = speed + 5;
            //lol it faster now
            GetComponent<AudioSource>().PlayOneShot(pickUpSound);
            //makes pickups make a sound
        }
    }
    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 17)
        {
            winText.text = "You Win!";
        }
    }
    private void OnCollisionStay(Collision collision)
    {
        if (!collision.gameObject.CompareTag("Ground"))
        {
            return;
        }
        isGrounded = true;
        //makes it so the ball knows when it's on the ground or not
    }
    private void Update()
    {
        if(Input.GetButton("Jump") && isGrounded)
        {
            rb.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            isGrounded = false;
            //Makes mah ball jump yeye
        }
    }
}
